#!/bin/sh
FILE=.todo
touch $FILE 
COMMAND=$1 #the first argument (add remove check uncheck renum show or empty)
shift #discard the first argument

case $COMMAND in 
add)
	for X in "$@" # quotes for adding sentences
	do
	LASTLINE=$(tail -n 1 $FILE)
	NUMBER=$((${LASTLINE%% *} + 1))
	echo "$NUMBER  [ ] $X" >> $FILE
	done	
	;;
remove)
	for X in $@
	do
	touch tmp_file
	#-v to only keep the lines that do not contain the search term
	#-w whole word search so that typing 2 does not remove line 12
	grep -vw "$X" $FILE > tmp_file 
	mv tmp_file $FILE
        done
	;;
check)
	for X in $@
	do
	LINE=$(grep -w "$X" $FILE) #find the entire line 
	NUMBER=${LINE%% *} #keep only the beginning of the line
	ITEM=${LINE##*] } #only the end
	sed -i "s/$NUMBER  \[ \] $ITEM/$NUMBER  \[X\] $ITEM/g" $FILE
        done
	#echo -e "\e[9m$LINE\e[0m" >> $FILE #other way of checking items
	;;
uncheck)
	for X in $@
	do
	LINE=$(grep -w "$X" $FILE)
	NUMBER=${LINE%% *}
	ITEM=${LINE##*] }
	sed -i "s/$NUMBER  \[X\] $ITEM/$NUMBER  \[ \] $ITEM/g" $FILE
        done
	;;
renum)
	NEW_NUMBER=1
	cat $FILE | while read LINE
        do
        NUMBER=${LINE%% *}
	sed -i "s/\<$NUMBER\>/$NEW_NUMBER/" $FILE #\< \> to match whole word
	NEW_NUMBER=$((NEW_NUMBER+1))
	done
	;;
show)
	cat $FILE
	;;
empty)
	rm $FILE
	touch $FILE
	;;
help)
	echo "todo add thing1 thing2 #to add items to the list"
	echo "todo remove 1 thing2 #to remove item 1 and item containing thing2"
	echo "todo check 1 thing2 #to mark item as done"
	echo "todo uncheck 1 thing2 #to remove marks"
	echo "todo show"
	echo "todo empty"
	echo "todo renum"
	;;
*)
	echo "Invalid option: try todo help"
	;;
esac
